import { Link } from 'react-router-dom';
import './Logo.scss';

const Logo = () => (
  <div id="logo" className='logo'>
    <Link to="/" className='logo__link'>MölkKing</Link>
  </div>
);

export default Logo;

import './Title.scss';

const Title = ({text}) => {
  return (
    <h1 className='mainTitle'>{text}</h1>
  );
};

export default Title;
import Routing from '../Router/Router';
import './App.scss';

const App = () => {

  return (
    <div id="App" className={'theme-light'}>
      <Routing />
    </div>
  );
};

export default App;
